nl2br — Inserts HTML line breaks before all newlines in a string<br>
<?php
echo nl2br("Welcome\r\nThis is my HTML document", false);
?>
<br>
<?php
$string = "This\r\nis\n\ra\nstring\r";
echo nl2br($string);
?>
