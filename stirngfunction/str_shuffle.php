str_shuffle — Randomly shuffles a string
<br>

<?php
$str = 'farhad';
$shuffled = str_shuffle($str);

// This will echo something like: bfdaec
echo $shuffled;
?>
