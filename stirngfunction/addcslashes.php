
<?php
echo addcslashes('farhad[ ]', 'A..z');
// output:  \f\a\r\h\a\d\[ \]
// All upper and lower-case letters will be escaped
// ... but so will the [\]^_`
?>
