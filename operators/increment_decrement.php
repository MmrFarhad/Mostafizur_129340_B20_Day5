The example below shows the different results of using the different increment/decrement operators:
<br>

<?php

$a = 10;
echo ++$a;
echo "<hr>";
echo $a++;
echo "<hr>";


$z=15;
echo --$z; // outputs 4
echo "<hr>";


$i=5;
echo $i--; // outputs 5
echo "<hr>";
echo $i--; // outputs 5
?>