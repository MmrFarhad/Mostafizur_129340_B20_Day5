The PHP assignment operators are used to write a value to a variable.

The basic assignment operator in PHP is "=". It means that the left operand gets set to the value of the assignment expression on the right.
<br>

<?php
$a=20;
echo $a+=45;
echo "<hr>";

echo $a-=6;
echo "<hr>";

$c=50;
echo $c-=25;
echo "<hr>";

$d = 47;
echo $d*=3;;
echo "<hr>";

$f=150;
echo $f/=50;
echo "<hr>";

$h=68;
echo $h%=8;
echo "<hr>";

