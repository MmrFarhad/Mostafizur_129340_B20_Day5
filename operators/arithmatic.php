The example below shows the different results of using the different arithmetic operators:
<br>
<?php
$a=20;
$b=5;
echo ($a + $b); //addition output 25
echo "<hr>";
echo ($a - $b); //subtraction output 15
echo "<hr>";
echo ($a * $b); //multiplication output 100
echo "<hr>";
echo ($a/$b); //division output 4
echo "<hr>";
echo ($a % $b); //modulus output
echo "<hr>";

?>
